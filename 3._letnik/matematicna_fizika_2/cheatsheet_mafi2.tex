% !TeX spellcheck = sl_SI

\documentclass[10pt,landscape]{article}
\usepackage{multicol}
\usepackage{calc}
\usepackage{ifthen}
\usepackage[landscape]{geometry}
\usepackage{amsmath,amsthm,amsfonts,amssymb}
\usepackage{color,graphicx,overpic}
\usepackage{hyperref}
\usepackage{amsthm}
\usepackage{mathrsfs}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{icomma}
\usepackage{soul}
\usepackage{physics}

\pdfinfo{
	/Title (Uporabne formule iz matematične fizike 2 za fizike)
	/Author (Urban Duh)
	/Subject (Matematična fizika 2)}

% This sets page margins to .5 inch if using letter paper, and to 1 cm
% if using A4 paper. (This probably isn't strictly necessary.)
% If using another size paper, use default 1cm margins.
\ifthenelse{\lengthtest { \paperwidth = 11in}}
{ \geometry{top=.5in,left=.5in,right=.5in,bottom=.5in} }
{\ifthenelse{ \lengthtest{ \paperwidth = 297mm}}
	{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
	{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
}

% Turn off header and footer
\pagestyle{empty}

% Redefine section commands to use less space
\makeatletter
\renewcommand{\section}{\@startsection{section}{1}{0mm}%
	{-1ex plus -.5ex minus -.2ex}%
	{0.5ex plus .2ex}%x
	{\normalfont\large\bfseries}}
\renewcommand{\subsection}{\@startsection{subsection}{2}{0mm}%
	{-1explus -.5ex minus -.2ex}%
	{0.5ex plus .2ex}%
	{\normalfont\normalsize\bfseries}}
\renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
	{-1ex plus -.5ex minus -.2ex}%
	{1ex plus .2ex}%
	{\normalfont\small\bfseries}}
\makeatother

% Don't print section numbers
\setcounter{secnumdepth}{0}

\setlength{\parindent}{0pt}
\setlength{\parskip}{0pt plus 0.5ex}

%My Environments

% -----------------------------------------------------------------------

\begin{document}
\raggedright
\footnotesize
\begin{multicols}{3}
	
\section{PDE 1. reda}

$a(x, y) \pdv{u}{x} + b(x, y) \pdv{u}{y} = \Phi(x, y, u)$ \\ \medskip

Metoda karakteristik: postavimo se na karakteristiko $(x(s), y(s))$, kjer se 
PDE 
poenostavi v sistem ODE. \\ 
Ker velja $\dv{u}{s} = \pdv{u}{x} \dv{x}{s} + \pdv{u}{y} \dv{y}{s}$, postavimo: \\
$\dv{x}{s} = a(x, y)$ in $\dv{y}{s} = b(x, y)$ \\
$\implies$ $\dv{u}{s} = \Phi(x, y, u) = \Phi(s, u)$ \\ \medskip

\section{Klasifikacija PDE 2. reda}
	
$a \pdv[2]{u}{x} + 2b \pdv{u}{x}{y} + c \pdv[2]{u}{y} + (\text{nižji redi}) = 0$ \\ \medskip

Enačba karakteristike: $\dv{y}{x} = \frac{-b \pm \sqrt{b^2 - ac}}{a}$, rešitev $y_k(x)$ \\
1. kanonična oblika: $a = c = 0, 2b = 1$ \\
2. kanonična oblika: $b = 0, |a| = |c| = 1$ \\
Za pretvorbo v 1. KO uvedemo $\xi = y + y_k, \eta = y - y_k$. \\
Za pretvorbo v 2. KO uvedemo $\alpha = \frac{1}{2}(\xi + \eta), \beta = \frac{1}{2}(\xi - \eta)$ 
(hiperbolične) ali $\alpha = \frac{1}{2}(\xi + \eta), \beta = \frac{1}{2i}(\xi - \eta)$ 
(eliptične). \\ \medskip

\textbf{Tipi robnih pogojev}: \\
\vspace{-0.11cm}
\begin{itemize}[itemindent=-15pt]
	\itemsep-0.10em 
	\item Cauchy - dana $u$ in $\pdv{u}{\vb n}$ na $\Gamma$ \\
	\item Dirichlet - dan $u$ na $\Gamma$ \\
	\item Neumann - dan $\pdv{u}{\vb n}$ na $\Gamma$ \\
\end{itemize}
\vspace{-0.11cm}
\medskip

PDE z robnimi pogoji je stabilen, če majhna sprememba robnih pogojev vodi do majhne spremembe 
rešitve, torej če za omejen $D \ \exists \delta > 0 \, \exists \varepsilon > 0:$ $|u_1(\Gamma) - 
u_2(\Gamma)| < \delta$ $\implies$ $|u_1(t) - u_2(t)| < \varepsilon$. \medskip \\

Konkretno definiran problem ima takšne robne pogoje, da je rešitev enolična in stabilna. \medskip \\

\begin{tabular}{|c||c|c|}
	\hline
	Tip enačbe & Pogoj & Karakteristike  \\
	\hline
	\hline
	hiperbolična & $b^2 - ac > 0$ & 2 realni \\
	\hline
	eliptična & $b^2 - ac < 0$ & 2 kompleksni \\
	\hline
	parabolična & $b^2 - ac = 0$ & 1 realna \\
	\hline
\end{tabular}
\\ \medskip
\begin{tabular}{|c|c|}
	\hline
	2. kanonična oblika & Konkretni robni pogoji \\
	\hline
	\hline
	$\pdv[2]{u}{\xi} - \pdv[2]{u}{\eta} + \dots = 0$ & Cauchy, odprta 
	$\Gamma$ \\
	\hline
	$\pdv[2]{u}{\xi} + \pdv[2]{u}{\eta} + \dots = 0$ & Dirichlet ali Neumann, 
	zaprta $\Gamma$ \\
	\hline
	$\pdv[2]{u}{\xi} + \dots = 0$ & Dirichlet ali Neumann, 
	odprta $\Gamma$ \\
	\hline
\end{tabular}
\medskip

\section{Valovna enačba}

\subsection{Fizikalni primeri}

\subsubsection{Neraztegljiva gibka težka struna}

$\mu = \dv{m}{s}$, $f_\alpha = \dv{F_{\text{zun}_\alpha}}{s}$ \\ \medskip 

$\mu \pdv[2]{x}{t} = \pdv{s}(F(s) \pdv{x}{s}) + f_x$, \\
$\mu \pdv[2]{y}{t} = \pdv{s}(F(s) \pdv{y}{s}) + f_y$ in \\
$\dd s^2 = \dd x^2 + \dd y^2$ \medskip \\

Mirujoča struna ($\pdv[2]{x}{t} = 0$) za majhne naklone ($\dv{x}{s} \approx 1$, $\dv{y}{s} \ll 1$) 
in $f_x = 0$: \\
$\dd s \approx \dd x$ in $F(s) = konst.$ \\
$\pdv[2]{y}{t} = c^2 \pdv[2]{y}{x} + \frac{f_y}{\mu}$, \qquad $c^2 = \frac{F}{\mu}$ \\ \medskip 

Če je še $f_y = 0$: \\
$E = T + V = \frac{1}{2} \mu \int \left(\left(\pdv{y}{t}\right)^2 + c^2 \left(\pdv{y}{x}\right)^2 
\right) \dd x$ \\ \medskip

\subsubsection{Opna}

Opna leži v $xy$ ravnini, nakloni so majhni. \medskip \\

$\pdv[2]{z}{t} = c^2 \laplacian{z} + \frac{p}{\rho h}$, \qquad $c^2 = \frac{\gamma}{\rho h}$ 
\medskip \\

\subsubsection{Longitudinalno nihanje palice}

$\rho \pdv[2]{u}{t} = \pdv{x} \qty(E \pdv{u}{x}) + f_x$ \medskip \\

\subsubsection{Zvok v tekočini}

Zanemarimo $\eta = 0$ in $\vb{f} = 0$. \\
$\pdv[2]{\delta p}{t} = c^2 \laplacian{\delta p}$, \qquad $c^2 = \frac{1}{\chi_s \rho_0}$ \medskip 
\\

Ker je $\curl{\pdv{\vb v}{t}} = 0$ uvedemo potencial $\varphi$. \\
$\vb v = - \grad{\varphi}$ \\
$\delta p = \rho_0 \pdv{\varphi}{t}$ \\
$\delta \rho = \frac{1}{c^2} \rho_0 \pdv{\varphi}{t}$ \\
$\pdv[2]{\varphi}{t} = c^2 \laplacian{\varphi}$ \medskip \\

Robni pogoj ob trdi steni: $\vb v = 0$ $\implies$ $\pdv{\varphi}{\vb n} = 0$ $\implies$ 
$\pdv{\delta p}{\vb n} = 0$. \medskip \\

\subsection{Rešitve v 1D}

$c^2 \pdv[2]{u}{x} = \pdv[2]{u}{t}$ \\
Dve realni karakteristiki $p = x + ct$ in $r = x - ct$ \\
$\implies$ splošna rešitev $u(x, t) = g(x - ct) + h(x +ct)$ za neki $g, h$. \medskip \\

Za Cauchyjev robni pogoj ob $t = 0$ (d'Alembertova rešitev): \\
$u(x, t) = \frac{1}{2} \qty[u(x - ct, 0) + u(x + ct, 0)] + \frac{1}{2c} \int_{x - ct}^{x+ct} 
\pdv{u}{t} \qty(\xi, 0) \, \dd{\xi}$ \medskip \\

\subsubsection{Nehomogeno sredstvo}

\textbf{Struna iz dveh delov} \\
$u_1 = g_i(x - c_1 t) + g_r(x + c_1 t)$ \\
$u_2 = g_t(x - c_2 t)$ \\
Iz zveznosti $u$ in $\pdv{u}{x}$ v $x = 0$ dobimo: \\
$g_r(- s) = g_i(s) \frac{c_2 - c_1}{c_2 + c_1}$ \\
$g_t(s) = g_i\qty(\frac{c_1}{c_2} s) \frac{2c_2}{c_1 + c_2}$ \medskip \\

\textbf{Diskretna sila} \\
$\pdv[2]{u}{t} = c^2 \pdv[2]{u}{x} + \frac{F_y(t)}{\mu} \delta(x - x_0)$ \\
$\implies$ \quad $0 = c^2 \eval{\pdv{u}{x}}_{x_0^-}^{x_0^+} + \frac{F_y}{\mu}$ \medskip \\

\textbf{Diskretna masa} \\
Dodaten pogoj: $m \pdv[2]{u}{t}(x_0, t) = F \eval{\pdv{u}{x}}_{x_0^-}^{x_0^+}$ \\
Lastne funkcije so ortogonalne s skalarnim produktom: \\
$\ev{X_n, X_p} \propto \mu \int_0^l X_n(x) X_p(x) \dd{x} + m X_n(x_0) X_p(x_0)$ \medskip \\

\subsubsection{Končna struna}

$\omega_n = k_n c$ \\
$u(x, t) = \sum_n \qty(\gamma_n \cos(k_n x) + \delta_n \sin(k_n x)) \qty(\alpha_n \cos(\omega_n t) 
+ \beta_n \sin(\omega_n t))$ \medskip 
\\

\textbf{Nehomogena enačba (vsiljeno nihanje)} \\
$\pdv[2]{u}{t} = c^2 \pdv[2]{u}{x} + f(x, t)$ \\
Začetni pogoji $u(x, 0) = \varphi(x)$ in $\pdv{u}{t} \qty(x, 0) = \psi(x)$ \medskip \\

Razvijemo po lastnih funkcijah $X_n$: \\
$f(x, t) = \sum_n f_n(t) X_n(x)$ \\
$\varphi(x) = \sum_n \varphi_n X_n(x)$ \\
$\psi(x) = \sum_n \psi_n X_n(x)$ \\
in nastavimo $u(x, t) = \sum_n u_n(t) X_n(x)$ \\
$\implies$ $u_n(t) = \varphi_n \cos(\omega_n t) + \frac{\psi_n}{\omega_n} \sin(\omega_n t) + 
\frac{1}{\omega_n} \int_0^t \sin(\omega_n \qty[t - \tau]) f_n(\tau) \dd{\tau}$ \medskip \\

\textbf{Nehomogen robni pogoj} \\
$u(0, t) = \mu_1(t)$ in $u(l, t) = \mu_2(t)$ \\
Z nastavkom $u(x, t) = u_1(x, t) + \mu_1(t) + \frac{x}{l}(\mu_2(t) - \mu_1(t))$ dobimo 
nehomogeno enačbo s homogenimi robnimi pogoji: \\
$\pdv[2]{u_1}{t} = c^2 \pdv[2]{u_1}{x} + \tilde f(x, t)$ \medskip \\

\subsection{Rešitve v 3D}

$\pdv[2]{\varphi}{t} = c^2 \laplacian{\varphi}$ \\
Rešitve so linearna kombinacija $\qty{g(\vb k \cdot \vb r - ct)}$, vendar je različnih $\vb k$ 
zvezno neskončno in imamo integral. \\
Samo sferično simetrične rešitve: $\varphi(r, t) = \frac{f(r - ct)}{r} + \frac{g(r + ct)}{r}$ 
\medskip \\

\section{Difuzijska enačba}

\subsection{Toplotna enačba}

$\vb j = - \lambda \grad{T}$ \quad (Fourierov zakon) \\
$\div{\vb j} = q - \rho c \pdv{T}{t}$ \quad (kontinuitetna enačba, ohranitev energije) \\
$D \laplacian{T} + \frac{q}{\rho c} = \pdv{T}{t}$, \quad $D = \frac{\lambda}{\rho c}$ \\
Če imamo premikajočo se snov, dobimo substancialni odvod $\pdv{T}{t} \to \pdv{T}{t} + \qty(\vb v 
\cdot 
\grad) 
T$. \medskip \\

\subsection{Rešitve}

Naj bo $f(\vb r, t) = \frac{q}{\rho c}$ \medskip \\

$T(\vb r, t) = \int_{-\infty}^\infty \int_\mathcal{D} f(\vb r_0, \tau) G(\vb r, \vb r_0, t - \tau) 
\dd[3]{\vb r_0} \dd{\tau} + $ \\
\qquad \qquad $+ \int_\mathcal{D} T(\vb r_0, 0) G(\vb r, \vb r_0, t) \dd[3]{\vb r_0} - $ \\
\qquad \qquad $- D \int_{-\infty}^\infty \int_{\partial \mathcal{D}} T(\vb r_b, \tau) \pdv{G(\vb r, 
\vb r_b, t - \tau)}{\vb n_b} \dd{\tau} \dd{S_b}$ \medskip \\

V 1D: $G_\infty(x, x_0, t) = \frac{1}{\sqrt{4 \pi D t}} e^\frac{-\qty(x - x_0)^2}{4Dt} H(t)$ 
\medskip \\

Rešujemo lahko tudi s separacijo spremenljivk. \medskip \\

\subsection{Princip maksimalne vrednosti}

Maksimum rešitve difuzijske enačbe na končnem $D$ je dosežen ob $t = 0$ ali na $\partial D$. 
\medskip \\

\section{Laplaceova enačba}

$\laplacian{u} = 0$ na $D$ \\
Vrednost $u$ v točki je enaka povprečju v majhni okolici. \\
Maksimum in minimum $u$ sta dosežena na $\partial D$. \medskip \\

Rešujemo s separacijo spremenljivk. \medskip \\

Na krogu s polmerom $a$ in robnim pogojem $\eval{u}_{\partial D} = f(\varphi)$:
$u(r, \varphi) =\frac{1}{2 \pi} = \int_0^{2 \pi} \frac{a^2 - r^2}{a^2 - 2 a r \cos(\varphi - 
\varphi') + r^2} f(\varphi') \dd{\varphi '}$ \medskip \\

\subsection{Poissonova enačba}

$\laplacian{u} = \rho$ \medskip \\

Razvijemo po lastnih funkcijah Laplaceove enačbe $\{X_n(x)\}$:
$\rho(x, y) = \sum_m X_m(x) \rho_m(y)$ \\
$u(x, y) = \sum_m X_m(x) u_m(y)$ \\
in dobimo ODE za $u_m$. \medskip \\

Ali pa nastavimo $u(\vb r) = \sum_{nml} c_{nml} U_{nml}(\vb r)$, kjer so $U_{nml}(\vb r)$ lastne 
funkcije Helmholtzeve enačbe z enakimi robnimi pogoji. \medskip \\

\section{Sturm-Liouvillov problem}

Splošen diferencialni operator $\mathcal{L}$: \\
$\ev{v, u} = \int_a^b v^* u \dd{x}$ \\
$\ev{v, \mathcal{L}u} = \ev{\mathcal{L}^\dagger v, u}$ \\
Operator je sebi-adjungiran, če $\mathcal{L} = \mathcal{L}^\dagger$ in $D(\mathcal{L}) = 
D(\mathcal{L}^\dagger)$. \medskip \\

Imejmo sedaj $\mathcal{L}u = (p(x) u'(x))' -q(x) u(x)$ in \\
rešujemo $\mathcal{L}u + \lambda w(x) u(x) = 0$ za $w(x) \geq 0$ (S-L problem). \\
$\ev{u, \mathcal{L} v} - \ev{\mathcal{L}u, v} = \qty[p(x) (u^* v' - {u^*}' v)]_a^b$ \medskip \\

Imejmo robne pogoje $\alpha_a u(a) + \beta_a u'(a) = 0$ in $\alpha_b u(b) + \beta_b u'(b) = 0$. \\
Če je $\qty[a, b]$ končen in $p(x), q(x) \geq 0$ (regularni S-L problem):
\vspace{-0.11cm}
\begin{itemize}[itemindent=-15pt]
	\itemsep-0.10em 
	\item $\lambda$ realne, neskončne, diskretne, nedegenerirane
	\item lastne funkcije so ortogonalne in kompletne
	\item vsaka naslednja lastna funkcija ima ničlo več
\end{itemize}
\vspace{-0.06cm}
Če je interval neskončen (singularni S-L problem), nimamo nedegeneriranosti in zadnje lastnosti. \\
Če so robni pogoji periodični, imamo dvakratno degeneracijo. \medskip \\

Za $n >> 1$ velja: \\
$u_n(x) \sim \frac{1}{\sqrt[4]{w p}} \exp{\pm i \int_0^x \sqrt{\frac{\lambda_n w(y)}{p(y)}} 
\dd{y}}$ \\
$\lambda_n \sim \frac{n^2 \pi^2}{(b - a)^2} D_\mathrm{ef}$ \\
$D_\mathrm{ef} = \qty[\frac{1}{b - a} \int_a^b \sqrt{\frac{w(y)}{p(y)}} \dd{y}]^{-2}$ \medskip \\

\subsection{Helmholtzeva enačba}

Pri valovni in difuzijski enačbi s separacijo $u(\vb r, t) = u_n (\vb r) T_n (t)$ pridemo do: \\
$\laplacian u_n + k_n^2 u = 0$ \qquad (amplitudna oz. Helmholtzeva enačba) \\
$T_n = e^{-k_n^2 Dt}$ (difuzijska), \qquad $T_n = e^{-i \omega_n t}$ (valovna) \medskip \\

Naj bodo $u_n(\vb r)$ rešitve Helmholtzeve enačbe za homogene robne pogoje. Tedaj znamo rešiti: 
\smallskip \\
\textbf{Začetni problem}: $u(\vb r, 0) = f(\vb r)$ \\
$u(\vb r, t) = \sum_n \frac{\ev{u_n, f}}{\ev{u_n, u_n}} u_n(\vb r) T_n(t)$ \smallskip \\

\textbf{Nehomogeno enačbo}: npr. $\pdv{u}{t} = D \laplacian u + g(\vb r, t)$ \\
Nastavimo $u(\vb r, t) = \sum_n c_n(t) u_n(\vb r)$, \\
razvijemo $g(\vb r, t) = \sum_n g_n(t) u_n(\vb r)$ in \\
rešimo ODE za $c_n(t)$. \smallskip \\

\textbf{Nehomogene robne pogoje}: \\
Nastavimo $u = v + h$, kjer $h$ poskrbi za RP. Vstavimo v začetno enačbo ter rešimo dobljeno 
nehomogeno enačbo s homogenimi RP. \medskip \\

\subsubsection{Cilindrične koordinate}

$\laplacian u + \lambda u = 0$ \\
$u = R(r) Z(z) \Phi(\varphi)$ \medskip \\

$\Phi'' = - m^2 \Phi$ \\
$\Phi_m = A \cos(m \varphi) + B \sin(m \varphi)$, $m = 1, 2, \dots$ \\
$\Phi_0 = A + B \varphi$ \medskip \\

$Z'' = \pm \beta^2 Z$ \\
Dobimo linearno kombinacijo $\sin$ / $\cos$ ali $\mathrm{sh}$ / $\mathrm{ch}$. \medskip \\

$r^2 R'' + rR' + \qty[(\lambda \pm \beta^2) r^2 - m^2]R = 0$ \\
$\lambda \pm \beta^2 = 0$ $\implies$ $R_m = A r^m + B^{-m}$, $R_0 = A + B \ln r$ \\
$\lambda \pm \beta^2 = k^2 > 0$ $\implies$ $R_m = A J_m(kr) + B Y_m(kr)$ \\
$\lambda \pm \beta^2 = -k^2 < 0$ $\implies$ $R_m = A I_m(kr) + B K_m(kr)$ \medskip \\

\subsubsection{Sferične koordinate}

$\laplacian u + \lambda u = 0$ \\
$\laplacian u = \frac{1}{r^2} \pdv{r}\qty(r^2 \pdv{u}{r}) - \frac{L^2}{r^2} u$ \\
$L^2 = -\frac{1}{\sin \vartheta} \pdv{\vartheta} \qty(\sin \vartheta \pdv{\vartheta}) - 
\frac{1}{\sin^2 \vartheta} \pdv[2]{\varphi}$ \\
$u = R(r) \Theta(\vartheta) \Phi(\varphi) = R(r) Y(\vartheta, \varphi)$ \bigskip \\

$L^2 Y(\vartheta, \varphi) = l(l + 1) Y(\vartheta, \varphi)$ \medskip \\

$\Phi'' = -m^2 \Phi$ \\
$\Phi_m = A \cos(m \varphi) + B \sin(m \varphi)$, $m = 1, 2, \dots$ \\
$\Phi_0 = A + B \varphi$ \medskip \\

$\dv{t} \qty[\qty(1 - t^2) \dv{t}] \Theta + \qty[l(l+1) - \frac{m^2}{1 - t^2}] \Theta = 0$ \\
$\Theta_{lm} = A P_l^m(t) + B Q_l^m(t) = A P_l^m(\cos \vartheta) + B Q_l^m(\cos \vartheta)$ \\
Na celi sferi dovoljene $l = 0, 1, 2, \dots$, $m = 0, 1, 2, \dots, l$ \bigskip \\

$\frac{1}{r^2} \dv{r} \qty(r^2 \dv{R}{r}) + \lambda R - \frac{l(l + 1)}{r^2}R = 0$ \\
$\lambda = 0$ $\implies$ $R_l = A r^l + B r^{-(l + 1)}$ \\
$\lambda = k^2 > 0$ $\implies$ $R_l = A j_l(kr) + B y_l(kr)$ \\
$\lambda = - k^2 < 0$ $\implies$ $R_l = A i_l(kr) + B k_l(kr)$ \medskip \\

$j_l(x) = \sqrt{\frac{\pi}{2x}} J_{l + \frac{1}{2}}(x)$ \qquad $y_l(x) = \sqrt{\frac{\pi}{2x}} 
Y_{l + \frac{1}{2}}(x)$ \medskip \\

\section{Greenove funkcije}

Rešujemo $\mathcal{L}_{\vb r} u(\vb r) = f(\vb r)$ v neskončnem prostoru. \\
$\mathcal{L}_{\vb r} G(\vb r, \vb r_0) = \delta(\vb r - \vb r_0)$ \qquad (definicija Greenove 
funkcije) \\
$\implies$ $u(\vb r) = \int_\mathcal{D} G(\vb r, \vb r_0) f(\vb r_0) \dd[3]{\vb r_0} + u_h(\vb r)$  
\\
$\mathcal{L}_{\vb{r}} u_h(\vb r) = 0$ \qquad (rešitev homogene PDE) \medskip \\


$G(\vb r, \vb r_0) = G^*(\vb r_0, \vb r)$ \medskip \\

\subsection{Helmholtzeva enačba v $\infty$ prostoru}

$\laplacian G(\vb r, \vb r_0) + k^2 G(\vb r, \vb r_0) = \delta(\vb r - \vb r_0)$ \medskip \\

\subsubsection{2D prostor}

$k^2 = 0$ $\implies$ $G(\vb r, \vb r_0) = \frac{1}{2\pi} \ln\abs{\vb r - \vb r_0}$ \medskip \\

$k^2 > 0$: \\
potujoče valovanje $e^{-i\omega t}$ $\implies$ $G(\vb r, \vb r_0) = - \frac{i}{4} 
H_0^{(1)}(k\abs{\vb r - \vb r_0})$ \\
potujoče valovanje $e^{i\omega t}$ $\implies$ $G(\vb r, \vb r_0) = \frac{i}{4} 
H_0^{(2)}(k\abs{\vb r - \vb r_0})$ \\
stoječe valovanje $\implies$ $G(\vb r, \vb r_0) = \frac{1}{4} Y_0(k \abs{\vb r - \vb r_0})$ 
\medskip \\

$k^2 < 0$ $\implies$ $G(\vb r, \vb r_0) = - \frac{1}{2 \pi} K_0(k \abs{\vb r - \vb r_0})$ \medskip 
\\

\subsubsection{3D prostor}

$k^2 = 0$ $\implies$ $G(\vb r, \vb r_0) = - \frac{1}{4 \pi \abs{\vb r - \vb r_0}}$ \medskip \\

$k^2 > 0$: \\
potujoče valovanje $\implies$ $G(\vb r, \vb r_0) = -\frac{1}{4 \pi r} e^{\pm i kr}$ \\
stoječe valovanje $\implies$ $G(\vb r, \vb r_0) = - \frac{\cos(kr)}{2 \pi r}$ 
\medskip \\

$k^2 < 0$ $\implies$ $G(\vb r, \vb r_0) = - \frac{e^{-kr}}{4 \pi r}$ \medskip \\

\subsection{Končno območje}

Pri Dirichletu mora dodatno veljati $G(\vb r_b, \vb r_0) = 0$, $\forall \vb r_b \in \partial 
\mathcal{D}$. \\
Pri Neumannu mora dodatno veljati $\pdv{G (\vb r_b, \vb r_0)}{\vb n_b} = 0$, $\forall \vb r_b \in 
\partial \mathcal{D}$, vendar $\laplacian{G}(\vb r, \vb r_0) = \delta(\vb r - \vb r_0) - 
\frac{1}{V}$. \medskip \\

Rešitev Helmholtzeve enačbe izrazimo iz Greenove zveze: \\
$\int_\mathcal{D}\qty(u \laplacian v - v \laplacian u) \dd{V} = \int_{\partial \mathcal{D}}\qty(u 
\pdv{v}{\vb n} - v \pdv{u}{\vb n}) \dd{\vb S}$, \\
v katero vstavimo $v(\vb r_0) = G(\vb r, \vb r_0)$. \medskip \\

Metode iskanja $G$: \\
\vspace{-0.11cm}
\begin{itemize}[itemindent=-15pt]
	\itemsep-0.10em 
	\item Nastavimo $G = G_\infty + g$, kjer $g$ poskrbi za RP. \\
	\item Rešimo PDE iz definicije $G$ z razvojem po lastnih funkcijah. \\ \hspace{-15pt}Nastavimo 
	$G(\vb r, \vb r_0) = \sum_n c_n(\vb r_0) u_n(\vb r)$ $\implies$ \\ \hspace{-17pt} $G(\vb r, \vb 
	r_0) = \sum_n \frac{u_n^*(\vb r_0) u_n(\vb r)}{\lambda_n}$. \\
	\item Rešimo PDE iz definicije $G$ z lepljenjem homogenih rešitev.
\end{itemize}
\vspace{-0.11cm}
\medskip

\subsection{Integralska formulacija}

Rešujemo $\mathcal{L} u(\vb r) - V(\vb r) u(\vb r) = 0$, poznamo \\
Greenovo funkcijo $\mathcal{L}$, $\mathcal{L} G_0(\vb r, \vb r_0) = \delta(\vb r - \vb r_0)$ in \\
homogeno rešitev $\mathcal{L} h(\vb r) = 0$. Tedaj: \\
$u(\vb r) = h(\vb r) + \int G_0(\vb r, \vb r_0) V(\vb r_0) u(\vb r_0) \dd[3]{\vb r_0}$ \\
To tipično rešujemo s Picardovo iteracijo z začetnim nastavkom $u_0(\vb r) = h(\vb r)$. \medskip \\

\section{Variacijska formulacija PDE}

PDE $\mathcal{L} u = f$, kjer je $\mathcal{L}^\dagger = \mathcal{L}$, je ekvivalenten minimizaciji 
funkcionala ($\var S = 0$) $S = \frac{1}{2} \ev{\mathcal{L}u, u} - \ev{f, u}$.

\end{multicols}
\end{document}