% !TeX spellcheck = en_GB

\documentclass[10pt,landscape]{article}
\usepackage{multicol}
\usepackage{calc}
\usepackage{ifthen}
\usepackage[landscape]{geometry}
\usepackage{amsmath,amsthm,amsfonts,amssymb}
\usepackage{color,graphicx,overpic}
\usepackage{hyperref}
\usepackage{amsthm}
\usepackage{mathrsfs}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{icomma}
\usepackage{soul}
\usepackage{physics}

\pdfinfo{
	/Title (Useful formulas from General relativity)
	/Author (Urban Duh)
	/Subject (General relativity)}

% This sets page margins to .5 inch if using letter paper, and to 1 cm
% if using A4 paper. (This probably isn't strictly necessary.)
% If using another size paper, use default 1cm margins.
\ifthenelse{\lengthtest { \paperwidth = 11in}}
{ \geometry{top=.5in,left=.5in,right=.5in,bottom=.5in} }
{\ifthenelse{ \lengthtest{ \paperwidth = 297mm}}
	{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
	{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
}

% Turn off header and footer
\pagestyle{empty}

% Redefine section commands to use less space
\makeatletter
\renewcommand{\section}{\@startsection{section}{1}{0mm}%
	{-1ex plus -.5ex minus -.2ex}%
	{0.5ex plus .2ex}%x
	{\normalfont\large\bfseries}}
\renewcommand{\subsection}{\@startsection{subsection}{2}{0mm}%
	{-1explus -.5ex minus -.2ex}%
	{0.5ex plus .2ex}%
	{\normalfont\normalsize\bfseries}}
\renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
	{-1ex plus -.5ex minus -.2ex}%
	{1ex plus .2ex}%
	{\normalfont\small\bfseries}}
\makeatother

% Don't print section numbers
\setcounter{secnumdepth}{0}

\setlength{\parindent}{0pt}
\setlength{\parskip}{0pt plus 0.5ex}

% My Environments
% -----------------------------------------------------------------------

\begin{document}
\raggedright
\footnotesize
\begin{multicols}{3}

$c = 1$ \medskip

\section{Special relativity}

$x^\mu = (t, x, y, z)$ \qquad (spacetime coordinate)\\
$\eta_{\mu \nu} = \mqty[-1 & 0 & 0 & 0 \\ 0 & 1 & 0 & 0 \\ 0 & 0 & 1 & 0 \\ 0 &
0 & 0 & 1]$ \qquad (metric) \\
$(\dd s)^2 = \eta_{\mu\nu} \dd x^\mu \dd x^\nu$ \qquad (spacetime
interval) \\
$(\dd \tau)^2 = - (\dd s)^2$ \qquad (proper time) \medskip \\

\subsection{Lorentz transformation}

$x^{\mu'} = \Lambda^{\mu'}{}_\nu x^\nu$, \qquad $\Lambda \in O(3, 1)$ \\
$(\dd s)^2 = (\dd s')^2$ \quad $\iff$ \quad $\eta_{\mu \nu} =
\Lambda^{\mu'}{}_\mu \Lambda^{\nu'}{}_\nu \eta_{\mu' \nu'}$ \medskip \\

$\Lambda^\mu{}_{\nu'} \Lambda^{\nu'}{}_{\rho} = \delta^{\mu}{}_{\rho}$ \\
$\Lambda^{\mu'}{}_{\nu} \Lambda^{\nu}{}_{\rho'} = \delta^{\mu'}{}_{\rho'}$ \medskip \\

\subsection{Vectors and forms}

$\vb V = V^\mu \vu e_{(\mu)} \in T_p$ \qquad (vector in tangent space at $p$) \\
$V^{\mu'} = \Lambda^{\mu'}{}_{\nu} V^\nu$ \\
$\vu e_{(\mu)} = \Lambda^{\nu'}{}_{\mu} \vu e_{(\nu')}$ \medskip \\

$\hat{\vartheta}^{(\mu)}(\vu e_{(\mu)}) = \hat{\vartheta}^{(\mu)}\vu e_{(\mu)} =
\delta^{\nu}{}_{\mu}$ \qquad (dual basis) \\
$\omega = \omega_\mu \hat{\vartheta}^{(\mu)} \in T_p^*$ \qquad (differential
1-form at $x$) \\
$\omega(V) = \omega_\mu V^\mu$ \\
$\omega_{\mu'} = \Lambda^\nu{}_{\mu'} \omega_\nu$ \\
$\vartheta^{(\rho')} = \Lambda^{\rho'}{}_{\sigma} \vartheta^{(\sigma)}$ \medskip \\

$\partial_\mu = \pdv{x^\mu}$ \medskip \\

\subsection{Tensors}

$T: (\cross_{k \ \text{times}} T_p^*) \cross (\cross_{l \ \text{times}} T_p)$
\qquad (rank $(k, l)$ tensor) \\
$T = T^{\mu_1 \dots \mu_k}{}_{\nu_1 \dots \nu_l} \vu e_{(\mu_1)} \otimes \cdots
\otimes \vu e_{(\mu_k)} \otimes \hat \vartheta^{(\nu_1)} \otimes \cdots \otimes
\hat \vartheta^{(\nu_l)}$ \\ 
$T^{\mu_1' \dots \mu_k'}{}_{\nu_1' \dots \nu_l'} = \Lambda^{\mu_1'}{}_{\mu_1}
\cdots \Lambda^{\mu_k'}{}_{\mu_k} \Lambda^{\nu_1}{}_{\nu_1'} \cdots
\Lambda^{\nu_l}{}_{\nu_l'} T^{\mu_1 \dots \mu_k}{}_{\nu_1 \dots \nu_l}$ \medskip \\

$\eta^{\mu \nu} \eta_{\nu \rho} = \eta_{\rho \nu} \eta^{\nu \mu} =
\delta^{\mu}{}_{\rho} = \delta_{\rho}{}^\mu$ \\
$V_\mu = \eta_{\mu \nu} V^{\nu}$ \\
$\omega^\mu = \eta^{\mu \nu} \omega_\nu$ \medskip \\

$T_{(\mu_1 \dots \mu_n) \rho}{}^{\sigma} = \frac{1}{n!} \qty(T_{\mu_1
\dots \mu_n \rho}{}^{\sigma} + \ \text{sum of permut.\ of } \mu_1
\dots \mu_n)$ \\
$T_{[\mu_1 \dots \mu_n] \rho}{}^{\sigma} = \frac{1}{n!} \qty(T_{\mu_1
\dots \mu_n \rho}{}^{\sigma} + \ \text{alternating sum of p.'s })$ \medskip \\

\subsection{Maxwell's equations}

$j^\mu = (\rho, j^i)$ \\
$A^\mu = (\phi, A^i)$ \\
$F^{\mu \nu} = \partial^\mu A^\nu - \partial^\nu A^\mu$ \\
$F^{0i} = -F^{i0} = E^i$ \qquad $F^{ij} = \tilde \varepsilon^{ijk} B_k$ 
$\tilde F = \frac{1}{2} \tilde \varepsilon^{\mu \nu \rho \sigma} F_{\rho \sigma}
= \tilde \varepsilon^{\mu \nu \rho \sigma} \partial_\rho A_\sigma$ \medskip \\

$\partial_\mu F^{\nu \mu} = j^\nu$ \qquad (Ampere and Gauss law) \\
$\partial_{[\mu} F_{\nu \lambda]} = 0$ \quad $\iff$ \quad $\partial_\mu \tilde
F^{\mu \nu} = 0$ \qquad (Bianchi identity) \medskip \\

\subsection{Energy and momentum}

$\gamma = (1-u^2)^{-1/2}$ \\
$u^\mu = \dv{x^\mu}{\tau} = (\gamma, \gamma u^i)$ \\
$u^\mu u_\mu = -1$ \medskip \\

$p^\mu = m u^\mu = (E, \gamma m u^i) = (E, p^i)$ \\
$p^\mu p_\mu = -m^2$ \\
$E^2 = m^2 + \vb p^2$ \medskip \\

$F^\mu = m \dv{u^\mu}{\tau}$ \medskip \\

\subsection{Classical field theory}

$L = \int \dd[3]{x} \mathcal{L}(\Phi, \partial_\mu \Phi)$ \qquad (Lagrangian) \\
$S = \int \dd{t} L = \int \dd[4]{x} \mathcal{L}(\Phi, \partial_\mu \Phi)$ \qquad
(action) \\
The equations of motion correspond to the extremum of $S$: \\
$\pdv{\mathcal L}{\Phi} - \partial_\mu \qty(\pdv{\mathcal{L}}{(\partial_\mu
\Phi)}) = 0$ \medskip \\

$\mathcal{L}_{EM}(A_\mu, \partial_\nu A_\mu) = -\frac{1}{4} F_{\mu \nu} F^{\mu
\nu} + A_\mu J^\mu$ \qquad (Maxwell Lag.) \medskip \\

\textbf{Noether theorem}: If equations of motions are invariant under an
infinitesimal global continuous transformation $\phi \to \phi + \alpha \Delta
\phi$, where $\mathcal L \to \mathcal L + \alpha \partial_\mu \mathcal J^\mu$,
then:
\vspace{-0.10cm}
\begin{itemize}[itemindent=-13pt]
	\itemsep-0.1em 
    \item $j^\mu = \pdv{\mathcal L}{\qty(\partial_\mu \phi)} \Delta \phi - \mathcal
        J^\mu$, \quad $\partial_\mu j^\mu = 0$, \\
    \item $Q = \int j^0 \dd[3]{\vb x}$, \qquad $\dv{Q}{t} = 0$.
\end{itemize}
\medskip

$T^\mu_{}_\nu = \frac{\partial \mathcal{L}}{\partial \qty(\partial_\mu \phi)} \partial_\nu \phi -
\mathcal L \delta^\mu_{}_\nu$ \qquad (energy-momentum tensor) \\
$\partial_\mu T^\mu_{}_\nu = 0$ \\
$E = \int T^{00} \dd[3]{\vb x}$ \\
$P^i = \int T^{0i} \dd[3]{\vb x} = - \int \pi \partial_i \phi \dd[3]{\vb x}$
\qquad (momentum) \medskip \\

\section{Manifolds and geometry}

\subsection{Gravity as geometry}

\textbf{Weak equivalence principle}: Inertial mass and gravitational mass are
equal. $\iff$ The motion of freely-falling particles is the same in a
gravitational field and a uniformly accelerate frame, in small enough regions of
spacetime. \medskip \\
\textbf{Einstein equivalence principle}: In small enough regions of spacetime,
the laws of physics reduce to those of special relativity; it is impossible to
detect the existence of a gravitational field by means of local experiments.
\medskip \\
$\implies$ There is no global inertial frame, we have to worked with curved
spacetime.

\subsection{Manifolds, vectors, forms, tensors}

See cheatsheet for Dodatna poglavja iz matematike. We are working in a
coordinate basis $x^\mu$. \medskip \\

$\partial_{\mu'} = \pdv{x^\mu}{x^{\mu'}} \partial_\mu$ \\
$\vb V = V^\mu \partial_\mu$ \\
$V^{\mu'} = \pdv{x^{\mu'}}{x^{\mu}} V^\mu$ \medskip \\

$\dd{x^\mu}(\partial_\nu) = \pdv{x^\mu}{x^\nu} = \delta^\mu_{}_\nu$ \\
$\dd{x^{\mu'}} = \pdv{x^{\mu'}}{x^\mu} \dd{x^\mu}$ \\
$\omega = \omega_\mu \dd{x^\mu}$ \\
$\omega_{\mu'} = \pdv{x^\mu}{x^{\mu'}} \omega_\mu$ \medskip \\

$T = T^{\mu_1 \dots \mu_k}_{}_{\nu_1 \dots \nu_l} \partial_{\mu_1} \otimes
\cdots \otimes \partial_{\mu_k} \otimes \dd{x^{\nu_1}} \otimes \cdots \otimes
\dd{x^{\nu_l}}$ \\
$T^{\mu_1' \dots \mu_k'}_{}_{\nu_1' \dots \nu_l'} =
\pdv{x^{\mu_1'}}{x^{\mu_1}} \cdots \pdv{x^{\mu_k'}}{x^{\mu_k}}
\pdv{x^{\nu_1}}{x^{\nu_1'}} \cdots \pdv{x^{\nu_l}}{x^{\nu_l'}} T^{\mu_1 \dots
\mu_k}_{}_{\nu_1 \dots \nu_l}$ \medskip \\

Commutator ($X, Y$ vector fields, $f$ a function): \\
$\comm{X}{ Y}(f) = X(Y(f)) - Y(X(f))$ \\
$\comm{X}{ Y}(a f + b g) = a \comm{X}{ Y}(f) + b \comm{X}{ Y}(g)$ \\
$\comm{X}{ Y}(fg) = f \comm{X}{ Y}(g) + g \comm{X}{ Y}(f)$ \\
$\comm{X}{ Y}^\mu = X^\lambda \partial_\lambda Y^\mu - Y^\lambda
\partial_\lambda X^\mu$ \medskip \\

\subsection{The metric}

The metric $g_{\mu \nu}$ is a symmetric $(0, 2)$ tensor. \\
We usually also demand $g = \det g_{\mu \nu} \neq 0$. \medskip \\

$g^{\mu \nu} g_{\nu \sigma} = g_{\lambda \sigma} g^{\lambda \mu} =
\delta^{\mu}_{}_{\nu}$ \\
$V_\mu = g_{\mu \nu} V^\nu$ \\
$\qty(\dd s)^2 = g_{\mu \nu} \dd{x^\mu} \dd{x^\nu}$ \medskip \\

Canonical form: $g_{\mu \nu} = \mathrm{diag}(-1, \dots, -1, 1, \dots, 1, 0, \dots, 0)$
\medskip \\

For any point $p$, we can find locally inertial coordinates, where:
$g_{\hat \mu \hat \nu}(p) = \eta_{\hat \mu \hat \nu}$, \qquad $\partial_{\hat \sigma}
g_{\hat \mu \hat \nu}(p) = 0$ \medskip \\

\subsection{Tensor densities}

$g = \det g_{\mu \nu}$ \medskip \\

$\tilde \varepsilon_{\mu \nu \rho \sigma}$ is not a tensor (does not transform
like a tensor), but a tensor density, $g$ is not a real scalar. \\
$\varepsilon_{\mu_1 \mu_2 \dots \mu_n} = \sqrt{\abs{g}} \tilde
\varepsilon_{\mu_1 \mu_2 \dots \mu_n}$ \qquad (this is a tensor) \\
$\varepsilon^{\mu_1 \mu_2 \dots \mu_n} = \frac{1}{\sqrt{\abs{g}}} \tilde
\varepsilon^{\mu_1 \mu_2 \dots \mu_n}$ \medskip \\

$\varepsilon^{\mu_1 \dots \mu_p \alpha_1 \dots \alpha_{n - p}}
\varepsilon_{\mu_1 \dots \mu_p \beta_1 \dots \beta_{n - p}} =$\\ \qquad$= (-1)^s p! (n - p)!
\delta^{[\alpha_1}_{}_{\beta_1} \cdots \delta^{\alpha_{n - p}]}_{}_{\beta_{n -
p}}$ \\
$s$ is the number of negative eigenvalues of the metric \medskip \\

\subsection{Integration}

$\dd[n]x' = \abs{\pdv{x^\mu'}{x^\mu}} \dd[n]x$ \medskip \\

Not all integrals are invariant. Invariant integral of a scalar: \\
$I = \int \phi(x) \sqrt{\abs{g}} \dd[n]{x}$ \medskip \\

\section{Curvature}

\subsection{Covariant derivative}

$\partial_\mu$ does not transform like a tensor in a general metric. \\
Covariant derivative $\nabla_\mu$:
\vspace{-0.10cm}
\begin{itemize}[itemindent=-13pt]
	\itemsep-0.1em 
    \item Transforms like a tensor, \\
    \item $\nabla(T + S) = \nabla T + \nabla S$, \\
    \item $\nabla(T \otimes S) = \nabla T \otimes S + T \otimes \nabla S$, \\
    \item $\nabla_\mu(T^\lambda_{}_{\lambda \rho}) = (\nabla
        T)_{\mu}^{}^{\lambda}_{}_{\lambda \rho}$, \\
    \item $\nabla_\mu \phi = \partial_\mu \phi$, where $\phi$ is a scalar.
\end{itemize}
\medskip
\columnbreak 

$\nabla_\mu V^\nu = \partial_\mu V^\nu + \Gamma^\nu_{\mu \lambda} V^\lambda$ \\
$\nabla_\mu \omega_\nu = \partial_\mu \omega_\nu - \Gamma^\lambda_{\mu \nu}
\omega_\lambda$ \\
$\nabla_\sigma T^{\mu_1 \dots \mu_k}_{}_{\nu_1 \dots \nu_l}= \partial_\sigma
T^{\mu_1 \dots \mu_k}_{}_{\nu_1 \dots \nu_l} + \Gamma^{\mu_1}_{\sigma
\lambda}T^{\lambda \mu_2 \dots \mu_k}_{}_{\nu_1 \dots \nu_l} + $\\ \qquad $\cdots -
\Gamma^{\lambda}_{\sigma \nu_1}T^{\mu_1 \dots \mu_k}_{}_{\lambda \nu_2 \dots
\nu_l} - \cdots$ \medskip \\

$S^{\lambda}_{}_{\mu\nu} = \Gamma^\lambda_{\mu\nu} - \hat
\Gamma^\lambda_{\mu\nu}$ \qquad is a tensor \\
$T^{\lambda}_{}_{\mu\nu} = \Gamma^\lambda_{\mu\nu} - \Gamma^\lambda_{\nu\mu}$
\qquad is a tensor (torsion) \medskip \\

In general relativity:
\vspace{-0.10cm}
\begin{itemize}[itemindent=-13pt]
	\itemsep-0.1em 
    \item $\nabla_\rho g_{\mu\nu} = 0$ (metric compatibility), \\
    \item $T^{\lambda}_{}_{\mu\nu} = 0$,
\end{itemize}
which gives: \\
$\Gamma^\sigma_{\mu\nu} = \frac{1}{2} g^{\sigma \rho}\qty(\partial_\mu g_{\nu
\rho} + \partial_\nu g_{\rho\mu} - \partial_{\rho}g_{\mu\nu})$ \qquad
(Christoffel conn.) \medskip \\

$\nabla_\lambda \varepsilon_{\mu \nu \rho \sigma} = 0$ \\
$\nabla_\rho g^{\mu\nu} = 0$ \medskip \\

$\nabla_\mu V^\mu = \frac{1}{\sqrt{\abs{g}}} \partial_\mu \qty(\sqrt{\abs{g}}
V^\mu)$ \\
Stokes theorem: $\int_\Sigma \nabla_\mu V^\mu \sqrt{\abs{g}} \dd[n]{x} =
\int_{\partial \Sigma} n_\mu V^\mu \sqrt{\abs{\gamma}} \dd[n - 1]{x}$\medskip \\

\subsection{Parallel transport and geodesics}

Directional covariant derivative along $x^\mu(\lambda)$:\\
$\frac{\mathrm{D}}{\dd \lambda} = \dv{x^\mu}{\lambda} \nabla_\mu$ \\
Equation of parallel transport along  $x^\mu(\lambda)$:\\
$\qty(\frac{\mathrm{D}}{\dd \lambda} T)^{\mu_1\dots\mu_k}_{}_{\nu_1\dots\nu_l} =
0$ \medskip \\

Parallel transport preserves norms: $\frac{\mathrm{D}}{\dd
\lambda}\qty(g_{\mu\nu}V^\mu W^\nu) = 0$ \medskip \\

A geodesic is a path that parallel-transports its own tangent vector. Geodesic
equation: \\
$\frac{\mathrm{D}}{\dd \lambda} \dv{x^\mu}{\lambda} = \dv[2]{x^\mu}{\lambda} +
\Gamma^{\mu}_{\rho \sigma} \dv{x^\rho}{\lambda} \dv{x^\sigma}{\lambda} = 0$
\medskip \\

If we are using the Christoffel connections, geodesics are also extremals of the
length functional $\implies$ geodesics are shortest paths on a manifold.
\medskip \\

Affine parameter: $\lambda = a \tau + b$ \medskip \\

For timelike and spacelike paths, the geodesic equation is equivalent to: \qquad  $u^\nu
\nabla_\nu u^\mu = 0$ \medskip \\

For a family of geodesics ($\gamma_s(\tau)$): \\
$T^\mu = \pdv{x^\mu}{\tau}$, \qquad $S^\mu = \pdv{x^\mu}{s}$ \\
$T^\sigma \nabla_\sigma \qty[ T^\rho \nabla_\rho S^\mu] = R^\mu_{}_{\nu \rho
\sigma} T^\nu T^\rho S^\sigma$ \qquad (geodesic deviation) \medskip \\

\subsection{Riemann curvature tensor}

$R^{\rho}_{}_{\sigma \mu \nu} = \partial_{\mu} \Gamma^{\rho}_{\nu \sigma} -
\partial_\nu \Gamma^{\rho}_{\mu \sigma} + \Gamma^{\rho}_{\mu \lambda}
\Gamma^\lambda_{\nu \sigma} - \Gamma^\rho_{\nu \lambda} \Gamma^\lambda_{\mu
\sigma}$ \medskip \\

$\comm{\nabla_\mu}{ \nabla_\nu} V^\rho = R^{\rho}_{}_{\sigma \mu \nu} V^\sigma -
T^\lambda_{}_{\mu\nu} \nabla_\lambda V^\rho$ \\
$R^{\rho}_{}_{\sigma\mu\nu} = -R^{\rho}_{}_{\sigma \nu \mu}$ \\
$R_{\rho \sigma \mu \nu} = - R_{\sigma \rho \mu \nu}$ \\
$R_{\rho \sigma \mu \nu} = R_{\mu \nu \rho \sigma}$ \\
$R_{\rho \sigma \mu \nu} + R_{\rho \mu \nu \sigma} + R_{\rho \nu \sigma \mu} =
0$ \\
$\nabla_{[\lambda} R_{\rho \sigma] \mu \nu} = 0$ \qquad (Bianchi identity) \\
In 4D, Riemann tensor has 20 independent components. \medskip \\

A coordinate system in which the components of the metric are
constant exists $\iff$ $R^\rho_{}_{\sigma \mu \nu} = 0$. \medskip \\

$R_{\mu \nu} = R^{\lambda}_{}_{\mu \lambda \nu}$ \qquad (Ricci tensor) \\
$R = R^\mu_{}_\mu$ \qquad (Ricci scalar) \\
$C_{\rho \sigma \mu \nu} = R_{\rho \sigma \mu \nu} - \frac{2}{n -
2}\qty(g_{\rho [\mu} R_{\nu] \sigma} - g_{\sigma [\mu} R_{\nu] \rho}) +$\\\quad $+
\frac{2}{(n - 1)(n - 2)} g_{\rho[\mu} g_{\nu] \sigma} R$ \qquad (Weyl tensor) \\
$G_{\mu \nu} = R_{\mu \nu} - \frac{1}{2} R g_{\mu \nu}$ \qquad (Einstein tensor)\\ \medskip

$\nabla^\mu G_{\mu \nu} = 0$ \medskip \\

\subsection{Killing vectors}

$\vb K$ is a Killing vector if $\nabla_{(\mu}K_{\nu)} = 0$ \\
$p^\mu \nabla_\mu \qty(K_\nu p^\nu) = 0$ \medskip \\

If $g_{\mu\nu}$ is independent of $x^{\sigma_*}$ $\implies$
$\partial_{\sigma_*}$ is a Killing vector. \medskip \\

Killing vectors correspond to continuous symmetries of the metric on a manifold. \medskip \\

$K$ is a Killing tensor if $\nabla_{(\mu} K_{\nu_1 \dots \nu_l)} = 0$ \\
$p^\mu \nabla_\mu \qty(K_{\nu_1 \dots \nu_l} p^{\nu_1} \cdots p^{\nu_l}) = 0$ \medskip \\

$\nabla_\mu \nabla_\sigma K^\rho = R^\rho_{}_{\sigma \mu \nu} K^\nu$ \\
$K^\mu \nabla_\mu R = 0$ \medskip \\

$J_{(T)}^\mu = K_\nu T^{\mu\nu}$ \qquad (for energy-momentum tensor) \\
$\nabla_\mu J^\mu_{(T)} = 0$ \medskip \\

\section{Gravity}

\subsection{Einstein's equation}

$G_{\mu \nu} + \Lambda g_{\mu \nu} = 8 \pi G T_{\mu \nu}$ \\
$\iff$ $R_{\mu \nu} - \Lambda g_{\mu \nu} = 8 \pi G \qty(T_{\mu \nu} - \frac{1}{2} T g_{\mu \nu})$
\medskip \\

$S = \int \sqrt{-g} \hat{\mathcal{L}}(\Phi, \nabla_\mu \Phi) \dd[4]{x} =$\\
\hspace{8pt}$= \frac{1}{16 \pi G}
\int \sqrt{-g} \qty(R -2 \Lambda) \dd[4]{x} + S_\mathrm{matter}$ \\
EL equations: $\pdv{\hat{\mathcal{L}}}{\Phi} - \nabla_\mu \qty(\pdv{\hat
{\mathcal{L}}}{\qty(\nabla_\mu \Phi)}) = 0$ \medskip \\

$T_{\mu \nu} = - \frac{2}{\sqrt{-g}} \fdv{S_\mathrm{matter}}{g^{\mu\nu}}$ \quad
(more consistent def.\ than Noether)

\subsection{Linearized gravity}

$g_{\mu\nu} = \eta_{\mu \nu} + h_{\mu \nu}$, \qquad $\abs{h_{\mu\nu}} \ll 1$ \\
$g^{\mu\nu} = \eta^{\mu \nu} - h^{\mu \nu}$ \medskip \\

$\Gamma^\rho_{\mu \nu} = \frac{1}{2} \eta^{\rho \lambda} \qty(\partial_\mu
h_{\nu \lambda} + \partial_\nu h_{\lambda \mu} - \partial_\lambda h_{\mu \nu})$
\\
$R_{\mu \nu \rho \sigma} = \frac{1}{2} \qty(\partial_\sigma \partial_\nu h_{\mu
\sigma} + \partial_\sigma \partial_\mu h_{\nu \rho} - \partial_\sigma
\partial_\nu h_{\mu \rho} - \partial_\rho \partial_\mu h_{\nu \sigma})$ \\
$R_{\mu \nu} = \frac{1}{2} \qty(\partial_\sigma \partial_\nu h^\sigma_{}_\mu +
\partial_\sigma \partial_\mu h^\sigma_{}_\nu - \partial_\mu \partial_\nu h -
\Box h_{\mu \nu})$ \\
$R = \partial_\mu \partial_\nu h^{\mu \nu} - \Box h$ \medskip \\ 

This theory is invariant under (any $\xi_\mu$): \\
$h_{\mu \nu} \to h_{\mu \nu} + \partial_\mu \xi_\nu + \partial_\nu \xi_\mu$
\medskip \\

$h_{00} = -2 \Phi$ \\
$h_{0i} = w_i$ \\
$h_{ij} = 2s_{ij} - 2\Psi \delta_{ij}$ \quad ($s_{ij}$ traceless) \\
{\tiny $\dd s^2 = - (1 + 2 \Phi) \dd t^2 + w_i(\dd t \dd x^i + \dd x^i \dd t) +
\qty[(1 - 2 \Psi) \delta_{ij} + 2 s_{ij}] \dd x^i \dd x^j$} \medskip \\

\textbf{Einstein's equations:} \\
$\laplacian \Psi = 4 \pi G T_{00} - \frac{1}{2} \partial_k \partial_l s^{kl}$ \\
$\qty(\delta_{jk} \laplacian - \partial_j \partial_k) w^k = -16 \pi G T_{0j} + 4
\partial_0 \partial_j \Psi + 2 \partial_0 \partial_k s_j^{}^k$ \\
$\qty(\delta_{ij} \laplacian - \partial_i \partial_j) \Phi = 8 \pi G T_{ij} +
\qty(\delta_{ij} \laplacian - \partial_i \partial_j - 2 \delta_{ij} \partial_0^2)
\Psi -$ \\ \qquad $-\delta_{ij} \partial_0 \partial_k w^k + \partial_0 \partial_{(i} w_{j)} +
\Box s_{ij} - 2 \partial_k \partial_{(i} s_{j)}^{}^k - \delta_{ij} \partial_k
\partial_l s^{jl}$ \medskip \\

\textbf{Transverse gauge}: \\
$\partial_i s^{ij} = 0$ \\
$\partial_i w^i = 0$ \medskip \\

\textbf{Synchronous gauge}: \\
$\Phi = 0$ \\
$w^i = 0$ \medskip \\

\textbf{Lorenz gauge}: \\
$\partial_\mu h^\mu_{}_\nu - \frac{1}{2} \partial_\nu h = 0$ \\
$\implies$ $\Box\qty(h_{\mu\nu} - \frac{1}{2} \eta_{\mu\nu} h) = -16 \pi
GT_{\mu\nu}$ \medskip \\

\subsection{Newtonian limit}

$T_{\mu \nu} = \rho u_\mu u_\nu = \text{diag}(\rho, 0, 0, 0)$ \qquad
(dust) \\
In transverse gauge we get: \\
$\dd s^2 = -(1 + 2 \Phi) \dd t^2 + (1 - 2 \Phi) \qty(\dd x^2 + \dd y^2 + \dd
z^2)$ \\
$\laplacian \Phi = 4 \pi G \rho$ \medskip \\

Photon trajectories: \\
$k^\mu = \dv{x^{(0)}^\mu}{\lambda}$, $l^\mu = \dv{x^{(1)}^\mu}{\lambda}$ \\
$\boldsymbol \alpha = -\frac{\Delta \vb l}{\abs{\vb k}} = 2\int \qty(\grad \Phi
- \frac{(\vb k \dot \grad \Phi) \vb k}{k^2}) \dd{(k\lambda)}
\xrightarrow[\infty]{} \frac{4GM}{b}$ \medskip \\

\subsection{Gravitational waves}

Transverse gauge and $T_{\mu \nu} = 0$: \\
$\Box s_{i j} = 0$, \qquad $\Phi = \Psi = w^i = 0$. \medskip \\

In the direction of $z$ axis: \\
$h_{\mu \nu} = \mqty(0 & 0 & 0 & 0 \\ 0 & C_{11} & C_{12} & 0 \\ 0 & C_{12} &
-C_{11} & 0 \\ 0 & 0 & 0 & 0) e^{-i\omega t + i \omega z}$ \medskip \\

\section{Schwarzschild solution}

\subsection{Schwarzschild metric}

$\dd{s}^2 = - \qty(1 - \frac{R_S}{r}) \dd{t} + \qty(1 - \frac{R_S}{r})^{-1}
\dd{r} + r^2 \qty(\dd{\vartheta}^2 + \sin^2 \vartheta \dd{\varphi}^2)$ \\
$R_S = 2GM$ \medskip \\

\textbf{Birkhoff's theorem}: the Schwarzschild metric is the unique vacuum
solution of Einstein's equation with spherical symmetry. \\
$\implies$ There are no time dependent solutions with spherical symmetry.
\medskip \\

$\lim_{r \to 0} R = \infty$ \quad $\implies$ \quad $r = 0$ is a real
singularity. \\
In $r = R_S$ no curvature scalars diverge $\implies$ $r = R_S$ is not a
singularity. \medskip \\

\subsection{Geodesics of Schwarzschild}

Schwarzschild metric has 3 Killing vectors because of spherical symmetry
(implying conservation of angular momentum) and one Killing vector corresponding
to time translation. \\
Conservation of momentum $\implies$ motion happens in a plane $\implies$ we
choose $\vartheta = \frac{\pi}{2}$. \medskip \\

$K^\mu = (\partial_t)^\mu$ \quad $\implies$ \quad $E = - K_\mu
\dv{x^\mu}{\lambda} = \qty(1 - \frac{R_S}{r}) \dv{t}{\lambda} = const.$ \\
$R^\mu = (\partial_\varphi)^\mu$ \quad $\implies$ \quad $L = R_\mu
\dv{x^\mu}{\lambda} = r^2 \dv{\varphi}{\lambda} = const.$ \\
$\varepsilon = - g_{\mu\nu} \dv{x^\mu}{\lambda} \dv{x^\nu}{\lambda} = const.$
\medskip \\

$\frac{1}{2} \qty(\dv{r}{\lambda})^2 + V(r) = \frac{1}{2} E^2$ \\
$V(r) = \frac{1}{2} \varepsilon - \varepsilon \frac{GM}{r} + \frac{L^2}{2r^2} -
\frac{GML^2}{r^3}$ \medskip \\

\subsection{Schwarzschild black holes}

Eddington-Finkelstein corrdinates: \\
$v = t + r^* = t + r + R_S \ln(\frac{r}{R_S} - 1)$ \\
$\dd{s}^2 = - \qty(1 - \frac{2GM}{r}) \dd{v}^2 + 2\dd{v} \dd{r} + r^2
\dd{\Omega}^2$ \medskip \\

Event horizon is a surface past which particles can never escape to infinity. \\
$r = R_S$ is an event horizon. \medskip \\

Kruskal-Szekres coordinates: \\
$T = \sqrt{\frac{r}{R_S} - 1} e^{r/2R_S} \sinh(\frac{t}{2R_S})$ \\
$R = \sqrt{\frac{r}{R_S} - 1} e^{r/2R_S} \cosh(\frac{t}{2R_S})$ \\
$-\infty < R < \infty$, \qquad $T^2 < R^2 + 1$ \\
$\dd{s}^2 = \frac{4 R_S^3}{r}e^{-r/R_S} \qty(-\dd{T}^2 + \dd{R}^2) + r^2
\dd{\Omega}^2$ \\
They are a maximal extension of the Schwarzschild solution. \medskip \\

\subsection{Metric inside a spherically symmetric static star}

$\dd{s}^2 = -e^{2\alpha(r)} \dd{t}^2 + e^{2 \beta(r)} \dd{r}^2 + r^2
\dd{\Omega}^2$ \\
$T_{\mu\nu} = (\rho + p) u_\mu u_\nu + p g_{\mu\nu}$ \\
$u_\mu = (e^\alpha, 0, 0, 0)$ \medskip \\

$m(r) = \frac{1}{2G} \qty(r - re^{-2\beta})$ \\
$\dv{m}{r} = 4 \pi r^2 \rho$, \qquad $M = m(R)$ \\
$\dv{\alpha}{r} = \frac{Gm(r) + 4\pi Gr^3 p}{r\qty[r - 2Gm(r)]}$ \\
$\dv{p}{r} = - \frac{(\rho + p) \qty[Gm(r) + 4 \pi Gr^3 p]}{r \qty[r - 2Gm(r)]}$
\\
To solve this, we need an equation of state $p = p(\rho)$. \medskip \\

Constant density gives $M_\mathrm{max} = \frac{4R}{9G}$. \medskip \\

\section{Black holes}

\textbf{No-hair theorem:} Stationary, asymptotically flat black hole solutions
to general relativity coupled to electromagnetism that are nonsingular outside
the event horizon are fully characterized by their mass, angular momentum and
electric charge (and magnetic charge). \medskip \\

$Q = - \int_\Sigma \dd[3]{x} \sqrt{\gamma} n_\mu J^\mu_e = - \int_{\partial
\Sigma} \dd[2]{x} \sqrt{\gamma^{(2)}} n_\mu \sigma_\nu F^{\mu\nu}$ \\
$E_R = \frac{1}{4 \pi G} \int_{\partial \Sigma} \dd[2]{x} \sqrt{\gamma^{(2)}} n_\mu
\sigma_\nu \nabla^\mu K^\nu$, \quad where $K$ is timelike \\
$J = -\frac{1}{8 \pi G} \int_{\partial \Sigma} \dd[2]{x} \sqrt{\gamma^{(2)}}
n_\mu \sigma_\nu \nabla^\mu R^\nu$, \quad where $R$ is rotational \medskip \\

\subsection{Charged (Reissner-Nordstr\" om) black hole}

$T_{\mu\nu} = F_{\mu\rho}F_\nu^{}^\rho - \frac{1}{4} g_{\mu\nu}
F_{\rho\sigma}F^{\rho\sigma}$ \\
$\dd{s}^2 = -\Delta \dd{t}^2 + \Delta^{-1} \dd{r}^2 + r^2 \dd{\Omega}^2$ \\
$\Delta = 1 - \frac{2GM}{r} + \frac{G (Q^2 + P^2)}{r^2}$ \medskip \\

Event horizons: \\
$r_\pm = GM \pm \sqrt{G^2 M^2 - G(q^2 + P^2)}$ \medskip \\

\subsection{Rotating (Kerr) black hole}

$\dd{s}^2 = - \qty(1 - \frac{2GMr}{\rho^2}) \dd{t}^2 - \frac{2GMar
\sin^2\vartheta}{\rho^2} 2\dd{t}\dd{\varphi} + \frac{\rho^2}{\Delta} \dd{r}^2
+$\\ \hspace{25pt}$+\rho^2 \dd{\vartheta}^2 + \frac{\sin^2 \vartheta}{\rho^2} \qty[(r^2 + a^2)^2 -
a^2 \Delta \sin^2 \vartheta] \dd{\varphi}^2$ \\
$\Delta = r^2 - 2GMr + a^2$ \\
$\rho^2 = r^2 + a^2 \cos^2\vartheta$ \\
$a = J/M$ \medskip \\

Event horizons: \\
$r_\pm = GM \pm \sqrt{G^2 M^2 - a^2}$ \medskip \\

Charge can be added by $2GMr \to 2GMr - G(Q^2 + P^2)$ \medksip \\


\vfill\null
\columnbreak
\vfill\null


\end{multicols}
\end{document}
