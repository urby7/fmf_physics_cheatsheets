% !TeX spellcheck = en_GB

\documentclass[10pt,landscape]{article}
\usepackage{multicol}
\usepackage{calc}
\usepackage{ifthen}
\usepackage[landscape]{geometry}
\usepackage{amsmath,amsthm,amsfonts,amssymb}
\usepackage{color,graphicx,overpic}
\usepackage{hyperref}
\usepackage{amsthm}
\usepackage{mathrsfs}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{icomma}
\usepackage{soul}
\usepackage{physics}
\usepackage{tikz}

\pdfinfo{
	/Title (Useful formulas from statistical physics)
	/Author (Urban Duh)
	/Subject (Statistical physics)}

% This sets page margins to .5 inch if using letter paper, and to 1 cm
% if using A4 paper. (This probably isn't strictly necessary.)
% If using another size paper, use default 1cm margins.
\ifthenelse{\lengthtest { \paperwidth = 11in}}
{ \geometry{top=.5in,left=.5in,right=.5in,bottom=.5in} }
{\ifthenelse{ \lengthtest{ \paperwidth = 297mm}}
	{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
	{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
}

% Turn off header and footer
\pagestyle{empty}

% Redefine section commands to use less space
\makeatletter
\renewcommand{\section}{\@startsection{section}{1}{0mm}%
	{-1ex plus -.5ex minus -.2ex}%
	{0.5ex plus .2ex}%x
	{\normalfont\large\bfseries}}
\renewcommand{\subsection}{\@startsection{subsection}{2}{0mm}%
	{-1explus -.5ex minus -.2ex}%
	{0.5ex plus .2ex}%
	{\normalfont\normalsize\bfseries}}
\renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
	{-1ex plus -.5ex minus -.2ex}%
	{1ex plus .2ex}%
	{\normalfont\small\bfseries}}
\makeatother

% Don't print section numbers
\setcounter{secnumdepth}{0}

\setlength{\parindent}{0pt}
\setlength{\parskip}{0pt plus 0.5ex}

% My Environments
% -----------------------------------------------------------------------

\begin{document}
\raggedright
\footnotesize
\begin{multicols}{3}
	
\section{Basics}

\subsection{Entropy}

\textbf{Macrostate}: system with fixed TD variables  (e.\ g.\ $E, N, V$). \\
\textbf{Microstate}: a point in phase space. \\
\textbf{Postulate} (equal a priori probabilities): All microstates compatible with a given 
macrostate are equiprobable. 
\medskip \\

Time evolution tends towards the macrostate with largest number of microstates 
(\textbf{equilibrium state}). \medskip \\

\textbf{Ergodicity}: $\lim_{T\to \infty} \frac{1}{T} \int_0^T A\qty(\vb{x}(t)) \dd{t} = \int 
A(\vb x) \rho \dd{\Gamma}$ for all observables $A$ \\
\textbf{Typicality} (true for $d \gg 1$, smooth $A$): for a typical $\vb x \in \Gamma$, $A(\vb 
x)$ is close to $\ev{A}$. \\
The postulate is justified by ergodicity or typicality of systems. Because $T$ for ergodicity 
can in practice be large, typicality is a better argument. \medskip \\

$\Omega(E, N, V)$: number of distinguishable microstates corresponding to the macrostate with 
$E, N ,V$. \\
$S = k_B \ln \Omega$ \\
$\beta = \frac{1}{k_B T} = \qty(\pdv{\ln \Omega}{E})_{N, V}$ \\
In equilibrium, all parts of the system that are in thermal contact (can exchange $E$) have the 
same $\beta$. 
\medskip \\

\subsubsection{Shannon entropy}

Given a probability distribution $p_j$, entropy $S$ must satisfy: 
\vspace{-0.10cm}
\begin{enumerate}[itemindent=-13pt]
	\itemsep-0.1em 
	\item $\exists i: p_i = 1$ $\implies$ $S = 0$ \\
	\item $\forall i: p_i = \frac{1}{\Omega}$ $\implies$ $S$ is maximal and $S$ is an increasing 
	\\ \hspace{50pt} function of $\Omega$ \\
	\item additivity: $p_{ij} = p_i p_j$ $\implies$ $S(\qty{p_{ij}}) = S(\qty{p_i}) + 
	S(\qty{p_j})$
\end{enumerate}
$\implies$ $S = \sum_{j} - p_j \ln p_j$ \medskip \\

\textbf{Principle of maximum entropy}: The probability distribution which best represents the 
current state of knowledge about a system 
is the one with largest entropy, in the context of precisely stated prior data. \medskip \\

Thermodynamic entropy is Shannon entropy of the uniform distribution ($p_i = \frac{1}{\Omega}$).

\subsection{Microcanonical ensemble}

Independent variables (fixed) $E, N, V$ $\implies$ $S = S(E, N, V)$. \medskip \\

$\rho(E)$ is constant on an interval $\Delta E$ around $E$ and $0$ otherwise. \medskip \\

Definitions of intensive quantities (constant across the whole system in equilibrium): \\
$\qty(\pdv{S}{E})_{N, V} = \frac{1}{T}$ \\
$\qty(\pdv{S}{N})_{E, V} = - \frac{\mu}{T}$ \\
$\qty(\pdv{S}{V})_{E, N} = \frac{p}{T}$ \medskip \\

$\implies$ $\dd{E} = T \dd{S} + \mu \dd{N} - p \dd{V}$ \\
$E$ is minimal in the equilibrium. \medskip \\

\subsection{Thermodynamic potentials}

\textbf{Helmoholtz free energy}: $F(T, N, V) = E - TS$ \\
$\dd{F} = - S \dd{T} - p \dd{V} + \mu \dd{N}$ \\ 
$F$ is minimal in the equilibrium. \medskip \\

\textbf{Gibbs free energy}: $G(T, p, V) = E - TS + pV$ \\
$\dd{G} = - S \dd{T} + V \dd{p} + \mu \dd{N}$ \\ 
$G$ is minimal in the equilibrium. \medskip \\

\textbf{Enthalpy}: $H(S, p, V) = E + pV$ \\
$\dd{H} = T \dd{S} + V \dd{p} + \mu \dd{N}$ \\ 
$H$ is minimal in the equilibrium. \medskip \\

\subsection{Canonical ensemble}

Independent variables (fixed) $T, N, V$. \medskip \\

$P(\text{system in state } j) = \frac{e^{-\beta E_j}}{Z}$ \\
$Z(T, N, V) = e^{- \beta F} = \sum_j e^{- \beta E_j} = \int g(E) e^{- \beta E} \dd{E}$ \\
$\dd{N} = g(E) \dd{E}$ \\
$\ev{E} = \pdv{\beta F}{\beta}$ \medskip \\

Quantum density matrix notation: \\
$\rho = \frac{1}{Z} e^{- \beta H}$ \\
$Z = \tr e^{- \beta H}$ \medskip \\

\subsection{Grand canonical ensemble}

Independent variables (fixed) $T, \mu, V$. \medskip \\

$P(\text{system in state } j) = \frac{e^{-\beta (E_j - \mu 
N_j)}}{Q}$ \\
$Q = \sum_j e^{-\beta (E_j - \mu N_j)} = \sum_{N_j} e^{\beta \mu N_j} Z(\beta, N_j, V) = 
\sum_{N_j} z^{N_j} Z$ \\
$\dd{q} = \dd(\ln Q) = \frac{\beta \ev{E}}{T} \dd{T} + \beta \ev{N} \dd{\mu} + \beta p \dd{V}$ 
\\
$\ev{E} = -\pdv{q}{\beta}$ \medskip \\

\section{Phase transitions}

Infinitesimal change in one parameter qualitatively changes properties of the system. \\
\textbf{PT of the $\mathbf{n}$-th order}: discontinuity in the $n$-th derivative of some 
thermodynamic potential. \medskip \\

\textbf{Mermin-Wagner theorem}: Continuous symmetry in systems with sufficiently short range 
interaction and $d \leq 2$ cannot be spontaneously broken at $T > 0$. \medskip \\

\textbf{Perron-Frobenius theorem}: $A \in \mathbb{R}^{N \times N}$, if $A_{ij} > 0$ $\forall i, 
j$: 
\\
\vspace{-0.10cm}
\begin{enumerate}[itemindent=-13pt]
	\itemsep-0.1em 
	\item largest eigenvalue is real, positive and non-degenerate, \\
	\item eigenvector corresponding to $\lambda_\text{max}$ has positive components.
\end{enumerate}
\vspace{-0.10cm}
\medskip

\subsection{Phase transitions in 1D}

$H = \sum_{i, j} J_{ij} \sigma_i^z \sigma_j^z$ \\
$J_{ij} \propto \frac{1}{\abs{\vb r_i - \vb r_j}^\sigma} = \frac{1}{r^\sigma} = J(r)$ \medskip \\

$\sum_r r J(r) < \infty$ (e.\ g.\ $\sigma > 2$) $\implies$ no PT at $T \neq 0$ \\
$1 < \sigma < 2$ $\implies$ PT at $T \neq 0$ \\
$1 < \sigma < \frac{3}{2}$ $\implies$ same critical exponents as MFA \medskip \\

\subsection{Kitaev model}

\begin{center}
\newcommand*{\N}{6}%
\begin{tikzpicture}[scale=0.5]
	\draw[step=1.0] (0,0) grid +(\N,\N);
	
	
	\foreach \i in {0, 1,...,\N} {
		\foreach \j in {0.5, 1.5,..., \N} {
			\fill (\i, \j) circle[radius=2pt];
			\fill (\j, \i) circle[radius=2pt];
		}
	}
	
	% v
	\foreach \pt in {(1, 4.5), (1, 5.5), (0.5, 5), (1.5, 5)} {
		\fill[red] \pt circle[radius=2pt];
	}
	\draw[very thick, red] (1, 4.5) -- (1, 5.5);
	\draw[very thick, red] (0.5, 5) -- (1.5, 5);
	\node[anchor=south west] at (1, 5) {$v$};
	
	% p
	\foreach \pt in {(1, 3.5), (2, 3.5), (1.5, 3), (1.5, 4)} {
		\fill[red] \pt circle[radius=2pt];
	}
	\draw[step=1.0, very thick, red] (1,3) grid (2,4);
	\node at (1.5, 3.5) {$p$};
	
	% c
	\draw[very thick, cyan] (3.5, 0) -- (3.5, \N);
	\foreach \i in {0,...,\N} {
		\fill[cyan] (3.5, \i) circle[radius=2pt];
	}
	\node[anchor=south] at (3.5, \N) {$C$};
	
	% c'
	\draw[very thick, cyan] (0, 0.5) -- (\N, 0.5);
	\foreach \i in {0,...,\N} {
		\fill[cyan] (\i, 0.5) circle[radius=2pt];
	}
	\node[anchor=west] at (\N, 0.5) {$C'$};
	
	% \bar c
	\draw[very thick, magenta] (5, 0) -- (5, \N);
	\foreach \i in {0.5,...,\N} {
		\fill[magenta] (5, \i) circle[radius=2pt];
	}
	\node[anchor=south] at (5, \N) {$\bar C$};
	
	% \bar c'
	\draw[very thick, magenta] (0, 2) -- (\N, 2);
	\foreach \i in {0.5,...,\N} {
		\fill[magenta] (\i, 2) circle[radius=2pt];
	}
	\node[anchor=west] at (\N, 2) {$\bar C'$};
	
	% interceptions
	\fill[yellow] (3.5, 2) circle[radius=2pt];
	\fill[yellow] (5, 0.5) circle[radius=2pt];
\end{tikzpicture}
\end{center}

$A_v = \prod_{j \in v} \sigma_j^x$ \\
$B_p = \prod_{j \in p} \sigma_j^z$ \\
$H = -J \sum_{v} A_v - J \sum_{p} B_p$, \qquad $J > 0$ \medskip \\

$\comm{A_v}{ A_{v'}} = \comm{B_p}{ B_{p'}} = \comm{A_v}{ B_p} = 0$ \\
$\comm{A_v}{ H} = \comm{B_p}{ H} = 0$ \medskip \\

$X_C = \prod_{j \in C} \sigma_j^x$, \qquad \qquad $X_{C'} = \prod_{j \in C'} \sigma_j^x$ \\
$Z_{\bar C} = \prod_{j \in \bar C} \sigma_j^z$, \qquad \hspace{15.5pt} $X_{\bar C'} = 
\prod_{j \in \bar 
C'} \sigma_j^z$ \medskip \\

$\comm{X_C}{ X_{C'}} = \comm{Z_{\bar C}}{ Z_{\bar C'}} = \comm{X_C}{ Z_{\bar C}} = 
\comm{X_{C'}}{ Z_{\bar C'}} = 0$ \\
$\acomm{X_{C}}{Z_{\bar C'}} = \acomm{X_{C'}}{Z_{\bar C}} = 0$ \\
All $X$ and $Z$ operators commute with all $A$, $B$ and $H$. \medskip \\

\subsubsection{Ground states}

$\ket{\psi_0} = \ket{\uparrow, \uparrow, \dots, \uparrow}$ \\
$P = \prod_v \frac{1 + A_v}{\sqrt{2}}$ \\
Ground states (also eigenstates of $A_v$, $B_p$ with eigenvalue $1$): \\
$\ket{\varphi_1} = P \ket{\psi_0}$, \qquad \qquad \hspace{13pt} $\ket{\varphi_2} = P X_C 
\ket{\psi_0}$, 
\\
$\ket{\varphi_3} = P X_{C'} \ket{\psi_0}$, \qquad \qquad $\ket{\varphi_4} = P X_C X_{C'} 
\ket{\psi_0}$ 
\medskip \\

In the GS subspace, $X$ and $Z$ operator matrix elements do not change if we deform the loops 
into topologically equivalent ones (same kind of irreducible loops on a torus). \medskip \\

In the GS subspace we can identify: \\
$\ket{\varphi_1} \equiv \ket{\tilde 0 \tilde 0}$, \quad $\ket{\varphi_2} \equiv \ket{\tilde 1 
\tilde 0}$, \quad $\ket{\varphi_3} \equiv \ket{\tilde 0 \tilde 1}$, \quad $\ket{\varphi_4} 
\equiv \ket{\tilde 1 \tilde 1}$ \\
$X_C \equiv \tilde \sigma_1^x$, \qquad $X_{C'} \equiv \tilde \sigma_2^x$,\qquad $Z_{\bar C} 
\equiv \tilde \sigma_2^z$, \qquad $Z_{\bar C'} \equiv \tilde \sigma_1^z$ \\
$X$, $Z$ operator distinguish between GS $\implies$ they are global order parameters. \medskip \\

For any local operator $A^{(\text{loc})}$ in TD limit and on GS subspace: \\
$\comm{A^{(\text{loc})}}{ X_{C, C'}} = \comm{A^{(\text{loc})}}{ X_{\bar C, \bar C'}} = 0$ \\
$\mel{\varphi_i}{A^{(\text{loc})}}{\varphi_j} \propto \delta_{i,j}$ \\
$\implies$ Ground states are topologically protected. \medskip \\

\subsubsection{1st excited states}

Eigenstates of $2$ $A$ or $B$ with eigenvalue $-1$. \medskip \\

String operators: \\
$Z_{\bar t} = \prod_{j \in \bar t} \sigma_j^z$ \\
$X_t = \prod_{j \in t} \sigma_j^x$ \\
$t$, $\bar t$ are irreducible open contours on the lattice. \medskip \\

1st excited states: \\
$\ket{\psi_t} = X_t \ket{\varphi_j}$ \\
$\ket{\psi_{\bar t}} = Z_{\bar t} \ket{\varphi_j}$ \medskip \\

$X$ and $Z$ excited states can be thought of as a pair of quasi particles (at the end of the 
strings). Exchanging $X$ and $Z$ particles results in a wave function phase $e^{i \pi / 2}$ 
$\implies$ anyons. \medskip \\


\subsection{Ising model}

$H = - J \sum_{i, j \ \text{n.\ n.}} \sigma_j^z \sigma_i^z - b \sum_{j} \sigma_j^z$ \medskip \\

\subsubsection{1D Ising in the thermodynamic limit $N \to \infty$}

$\frac{F}{N} = - J - k_B T \ln(\cosh(\beta b) + \sqrt{\sinh^2(\beta b) + e^{-4\beta J}})$ \\
$\frac{M}{N} = \frac{1}{N} \sum_j \ev{\sigma_j^z} = - \frac{1}{N} \pdv{F}{b} = \frac{\sinh(\beta 
b)}{\sqrt{\sinh^2(\beta b) + e^{-4\beta J}}}$ \\
$\frac{\chi}{N} = \frac{1}{N} \pdv{M}{b} = \frac{\beta \sinh^2(\beta b) \cosh(\beta 
b)}{\qty(\sinh^2(\beta b) +e^{-4 \beta J})^\frac{3}{2}} + \frac{\beta \cosh(\beta 
b)}{\sqrt{\sinh^2(\beta b) + e^{-4\beta J}}}$ \medskip \\

At $b = 0$: \\
$g(r) = \ev{\sigma_i^z \sigma_{i + 1}^z} = \tanh^r(\beta J) = e^{- r / \xi}$ \\
$\xi = \frac{1}{\ln\coth(\beta J)}$ \medskip \\

At $b = 0$ and $T = 0$: \\
$M = 1$, \qquad $\chi \to \infty$, \qquad $\xi \to \infty$ \\
$\implies$ Phase transition of the $2$nd order. \\
There is no phase transition at $T > 0$. \medskip \\

\subsubsection{2D Ising with $b = 0$}

$n(r) =$ number of 
configurations containing closed loops with total circumference $r$ $= m^*(r)$ \\
$m(r) = $ number of configurations with $r$ flipped bonds $= n^*(r)$ \\
Where $^*$ denotes the quantities on the dual lattice. \\
$M$ is the number of bonds. \\
$K = \beta J$ \\
$\tanh K^* = e^{-2K}$ \qquad (definition) \medskip \\

$Z = \cosh(K)^M 2^N \sum_{r = 0}^\infty n(r) \tanh(K)^r =$\\ \hspace{6pt} $=e^{KM} \sum_{r = 
0}^\infty m(r) e^{-2Kr}$ \\
$Z(N, T) = 2^{-N^*} \qty(\sinh K^* \cosh K^*)^{-M/2} Z^*(N^*, T^*)$ \medskip \\

\textbf{Square lattice}: \\
Self dual, $N^* = N$, $M = 2N$ \\
$Z(N, T) = \sinh^{-N}(2K^*) Z(N, T^*)$ \\
$K_c = K_c^* = \frac{1}{2} \ln(1 + \sqrt{2})$ \\
PT is of the $2$nd order ($c$ diverges). \medskip \\

$\frac{\beta F}{N^2} = - \ln (2 \sinh(2K)) - \frac{1}{\pi} \int_0^{\pi/2} \ln \frac{1 + \sqrt{1 - 
g^2 \sin^2 \varphi}}{2} \dd{\varphi}$ \\
$g = \frac{2 \sinh(2K)}{\cosh\ln^2(2K)}$ \medskip \\

$\frac{M}{N^2} = \begin{cases}
	\qty(1 - \sinh^{-4}(2K))^{1/8}; &T < T_c \\
	0; &\text{otherwise}
\end{cases}$

\subsection{Critical exponents}

Order parameter $M$ and conjugate variable $h$. \\
$h = 0:$ $M \neq 0$ at $T < T_c$, $M = 0$ at $T > T_c$ \\
$h \neq 0:$ $M \neq 0$ \\
$t = 1 - \frac{T}{T_c}$ \medskip \\

Definitions of critical exponents: \\
$c \sim \abs{t}^{-\alpha}$, \qquad at $h = 0$ \\
$M = -\pdv{F}{h} \sim \abs{t}^\beta$, \qquad at $h = 0$, $T < T_c$ \\
$\chi = \frac{1}{V} \pdv{M}{h} \sim \abs{t}^{-\gamma}$, \qquad at $h = 0$ \\
$M \sim h^\frac{1}{\delta}$, \qquad at $T = T_c$ \\
$g(r) = \ev{M_i M_{i + r}} \sim \frac{1}{r^{d - 2 + \eta}} e^{- r / \xi}$ \\
$\xi \sim \abs{t}^{- \nu}$ \medskip \\

\textbf{Scaling hypothesis}: near $T = T_c$ $\xi$ is the only relevant scale of the system 
$\implies$ for any characteristic length $L \propto \xi \sim \abs{t}^{- \nu}$. \medskip \\

Scaling laws: \\
$\nu d = 2 - \alpha$ \\
$\alpha + 2\beta + \gamma = 2$ \\
$\gamma = \beta (\delta - 1)$ \\
$\gamma = \nu (2 - \eta)$ \medskip \\

Systems with the same critical exponents are in the same \textbf{universality class}. \medskip \\

\begin{center}
	\begin{tabular}{|c||c|c|}
		\hline
		& 2D Ising & MFA 2D Ising \\
		\hline \hline
		$\alpha$ & $0$ & $0$ \\
		\hline
		$\beta$ & $1/8$ & $1/2$ \\
		\hline
		$\gamma$ & $7/4$ & $1$ \\
		\hline
		$\delta$ & $15$ & $3$ \\
		\hline
		$\nu$ & $1$ & $1/2$ \\
		\hline
		$\eta$ & $1/4$ & $0$ \\
		\hline
	\end{tabular}
\end{center} \medskip

Ginzburg criterion for the validity of mean field approximation: 
$d > \frac{2 - \alpha}{\nu} = 4$ \medskip \\


\subsubsection{Landau theory}

Expand $G = G(M)$, equilibrium $G$ corresponds to min.\ $G(M)$. \medskip \\

\textbf{System with $\mathbf{\mathbb{Z}_2}$ symmetry} ($M \to -M$ symmetry): \\
$G(M) = a(T) + \frac{1}{2} b(T) M^2 + \frac{1}{4!} c(T) M^4 + \cdots$ \\
Expanding $b(T) \approx b_0 (T - T_c)$ and assuming $c = konst. > 0$ gives a $2$nd order PT 
with $M = \sqrt{\frac{b_0}{c} (T_c - T)}$. \\
$\implies$ Equivalent to MFA (same critical exponents). \medskip \\

If $c < 0$ we get a $1$st order PT. At $b = c = 0$, 1st and 2nd order PT lines touch $\implies$ 
\textbf{tricritical point}. \medskip \\

\textbf{Cubical term}: \\
$G(M) = \frac{1}{2} b(T) M^2 - \frac{1}{3!} d(T) M^3 + \frac{1}{4!} c(T) M^4 + \cdots$ \\
$M$ has discontinuity at $T_c$ $\implies$ $1$st order PT. \medskip \\

\subsection{Renormalization group}

$\xi \to \infty$ at $T_c$ $\implies$ scale invariance $\implies$ self-similarity \medskip \\

$\vb K = (K_1, K_2, \dots)$ \qquad (vector of constants) \\
Decimation (e.\ g.\ sum over every other spin): $H(\vb K) \to H'(\vb K')$ \\
$\vb K' = \mathcal{R}(\vb K)$ \qquad (RG map) \\
$\xi' = \xi / L$, \qquad $N' = N / L^d$ \medskip \\

Self-similarity $\implies$ system at $T_c$ corresponds to $\vb K^* = \mathcal{R}(\vb K^*)$. \\
Same universality class $\implies$ same set of $\vb K^*$. \\
$\Delta \vb K' = \eval{\pdv{\mathcal{R}}{\vb K}}_{\vb K^*} \Delta \vb K = A^* \Delta \vb K$ 
\medskip \\

Eigenvalue decomposition of $A^*$ into $\vb x_i, \lambda_i$. \\
$\Delta \vb K^{(n)} = \sum_i c_i \lambda_i^n \vb x_i$ \\
\vspace{-0.10cm}
\begin{itemize}[itemindent=-13pt]
	\itemsep-0.1em 
	\item $\abs{\lambda_i} > 1$: relevant variable (i.\ e.\ $t$, $h$), grows with $n$ \\
	\item $\abs{\lambda_i} < 1$: irrelevant variable, goes to zero with $n$ \\
	\item $\abs{\lambda_i} = 1$: marginal variable
\end{itemize}

$\lambda_i = L^{y_i}$ \\
We identify $t$ as the variable corresponding to $\lambda_1$. \\
$\nu = \frac{1}{\lambda_1}$ \medskip \\

Self-similarity $\implies$ $F(t, h) = F'(t', h')$ \\
$f(t, h) = \frac{F(t, h)}{N} = \frac{1}{L^d} f'(t', h') = \frac{1}{L^d} f(\lambda_1 t, \lambda_2 
h) = t^{d\nu} f\qty(\frac{h}{t^{\nu y_2}})$ \medskip \\

\section{Nonequilibrium statistical physics}

\subsection{Transport coefficients}

$A_i$ extensive quantities, $S = S(A_i)$ \\
$\dd{S} = \sum_i \pdv{S}{A_i} \dd{A_i} = \sum_i \gamma_i \dd{A_i}$ \\
$a_i = \dv{A_i}{V}$ \\
$\vb j_i = \frac{\dd{A_i}}{\dd{\vb S} \dd{t}}$ \\
$\pdv{a_i}{t} + \div{\vb j_i} = 0$ \medskip \\

Equilibrium: $\gamma_i$ constant over the whole system \\
Different $\gamma_i$ $\implies$ exchange of $A_i$ $\implies$ non-zero $j_i$ \\
$\vb j_i = \sum_j L_{ij} \grad \gamma_j = \sum_j L_{ij} \vb F_j$ \medskip \\

$\sigma_s = \sum_i \vb j_i \cdot \vb F_i = \sum_{ij} \vb F_i L_{ij} \vb F_j$ \\
$\vb j_s = \sum_i \gamma_i \vb j_i$ \\
$\dv{s}{t} = - \div \vb j_s + \sigma_s = \dv{s_\text{ext}}{t} + \dv{s_\text{int}}{t}$ \\
$\dv{s_\text{int}}{t} \geq 0$ \qquad (2nd law of TD) 
$\implies$ $L$ is positive semidefinite. \medskip \\

\subsubsection{Time reversal symmetry}

Time reversal operator: $\mathcal{T}$ \\
$\mathcal{T} A(t) \mathcal{T}^{-1} = \varepsilon_A A(-t)$ \\
$L_{ij}(\gamma_k) = \varepsilon_i \varepsilon_j L_{ji}(\varepsilon_k \gamma_k)$ \medskip \\

\subsection{Linear response}

$H(t) = H_0 - a(t) A$, \qquad $a(t < t_0) = 0$ \\
$\rho_\text{eq} = \frac{1}{Z} e^{- \beta H_0}$ \\
$A_I(t) = e^{i H_0 t / \hbar} A e^{-i H_0 t / \hbar}$ \medskip \\

$\rho(t) = \rho_\text{eq} + \delta \rho(t)$ \\
$\delta \rho(t) = \frac{i}{\hbar} \int_{t_0}^t a(t') \comm{A_I(t' - t)}{ \rho_\text{eq}} \dd{t'}$ \\
$\ev{B(t)} - \ev{B}_\text{eq} = \frac{i}{\hbar} \int_{t_0}^t a(t') \ev{\comm{B_I(t -t')}{ 
A_I}}_\text{eq} \dd{t'} =$ \\ \hspace{54pt} $=\int_{-\infty}^\infty \chi_{BA}(t - t') a(t') 
\dd{t'}$ \\
$\chi_{BA}(t) = \frac{i}{\hbar} \vartheta(t) \ev{\comm{B_I(t)}{ A_I}}_\text{eq}$ \medskip \\

$\chi_{BA}(\omega) = \int_{-\infty}^\infty \chi_{BA}(\tau) e^{i \omega \tau} \dd{\tau}$ \\
Generalised: $\chi_{BA}(\omega) = \lim_{\varepsilon \to 0^+}\int_{-\infty}^\infty 
\chi_{BA}(\tau) e^{i \omega \tau -\varepsilon t} \dd{\tau}$ \\
Kramers-Kronig: $\chi_{BA}(\omega) = \frac{1}{\pi i} \mathcal{P} \int_{-\infty}^\infty 
\frac{\chi_{BA}(\omega')}{\omega' - \omega} \dd{\omega'}$ \medskip \\

$\chi_{BA}'(\omega) = \Re \chi_{BA}(\omega)$, \qquad $\chi_{BA}''(\omega) = \Im \chi_{BA}(\omega)$ 
\medskip \\

$\chi_{BA}(t) = \varepsilon_A \varepsilon_B \chi_{AB}(t)$ \medskip \\

\subsubsection{Fluctuation-dissipation theorem}

Correlation functions: \\
$\xi_{BA}(t) = \frac{1}{2\hbar} \ev{\comm{B_I(t)}{ A_I}}_\text{eq}$ \\
$J_{BA}(t) = \ev{B_I(t) A_I}_\text{eq}$ \\
$S_{BA}(t) = \frac{1}{2} \ev{\acomm{B_I(t)}{ A_I}}_\text{eq}$ \\
$K_{BA}(t) = \frac{1}{\beta} \int_0^\beta \ev{e^{\lambda H_0} A_I e^{-\lambda H_0} 
B_I(t)}_\text{eq} 
\dd{\lambda}$ \medskip \\

Fluctuation-dissipation theorems: \\
$\xi_{BA}(\omega) = \chi_{BA}''(\omega)$ \\
$\frac{1}{2\hbar} \qty(1 - e^{-\beta \hbar \omega}) J_{BA}(\omega) = \chi_{BA}''(\omega)$ \\
$S_{BA}(\omega) = \hbar \coth(\frac{\beta \hbar \omega}{2}) \chi_{BA}''(\omega)$ \\
$K_{BA}(\omega) = \frac{2}{\beta \omega} \chi_{BA}''(\omega)$ \medskip \\

\subsection{Stochastic processes}

\subsubsection{Gaussian processes}

$F_i = F(t_i)$, \qquad $\vb x = \qty(F_1, F_2, \dots, F_n)$ \\
$\vb m = \ev{\vb x}$ \\
$F(t)$ is Gaussian if $p(\vb x) \propto \exp\qty(-\frac{1}{2} \qty(\vb x - \vb m) A \qty(\vb x 
- \vb m))$ \medskip \\

$\phi(\boldsymbol \xi) = \int p(\vb x) e^{i\vb x \cdot \boldsymbol \xi} \dd{\vb x} = \sum 
\frac{\qty(i \xi_1)^{r_1} \cdots \qty(i \xi_n)^{r_n}}{r_1! \cdots r_n!} \ev{x_1^{r_1} \cdots 
x_n^{r_n}}$ \\
For Gaussian $p$: $\phi(\boldsymbol{\xi}) = \exp\qty(i \boldsymbol{\xi} \cdot \vb m - 
\frac{1}{2} \boldsymbol \xi A^{-1} \boldsymbol \xi)$ \\
Cumulants (mean, variance, higher central moments) are generated by $\ln \phi$. \medskip \\

$g(t_i, t_j) = \ev{(x_i - m_i)(x_j - m_j)} = A^{-1}_{ij}$ \\
$F(t)$ is stationary if $m_i = m$ and $g(t_i, t_j) = g(t_i - t_j)$ \medskip \\

\subsubsection{Markovian processes}

A process is Markovian if $p(x_n = F_n | x_{n - 1} = F_{n - 1}, \dots, x_1 = F_1) = p (x_n = F_n 
| x_{n - 1} = F_{n - 1})$. \\
Stationary Gaussian $F(t)$ with $m(t) = 0$ and $g(\tau) \propto \delta(\tau)$ is Markovian. 
\medskip \\

Probability distribution for $v$: $f(v, t)$ \\
Probability to move by $w$ from $v - w$ in $\Delta t$: $p(v - w, w, \Delta t)$ \\
$f(v, t + \dd{t}) = \int f(v - w, t) p(v - w, w, \dd{t}) \dd{w}$ \\
$\ev{w^r} = \int w^r p(v, w, \dd{t}) \dd{w} = M_r \dd{t} + \mathcal{O}(t^2)$ \medskip \\

$\pdv{f(v, t)}{t} = \sum_{r = 1}^\infty \frac{(-1)^r}{r!} \pdv[r]{v} \qty[f(v, t) M_r]$ \medskip \\

\subsubsection{Brownian motion}

$\dv{v}{t} = -\gamma v + \frac{F(t)}{m}$ \\
$\ev{F(t)} = 0$, \qquad $\ev{F(t + \tau) F(t)} = 2D m^2 \delta(\tau)$ \\
$\implies v(t) = v_0 e^{-\gamma t} + \frac{1}{m}\int_0^t e^{-\gamma(t - \tau)} F(\tau) \dd{\tau}$ 
\medskip \\

$\ev{v(t)} = v_0 e^{- \gamma t}$ \\
$\sigma_v^2 = \frac{D}{\gamma} \qty(1 - e^{-2\gamma t})$ \\
Equipartition theorem $\implies$ $\gamma = \frac{mD}{kT}$ \medskip \\

$p(v, w, \dd{t}) = \frac{1}{\sqrt{4 \pi D \dd{t}}} \exp\qty(-\frac{\qty(w + \gamma v 
\dd{t})^2}{4D\dd{t}})$ \\
$M_1 = - \gamma v$, \qquad $M_2 = 2D$, \qquad others $0$ \\
$\implies$ $\pdv{f}{t} = \pdv{(f \gamma v)}{v} + \pdv[2]{(f D)}{v}$ \qquad (Fokker-Planck) \\
In the steady state ($\pdv{f}{t} = 0$): $f(v, t) \propto e^{- \frac{\gamma v^2}{2D}} = e^{- 
\frac{mv^2}{2kT}}$ \medskip \\

\subsection{Nonequilibrium fluctuation theorems}

Process between two equilibrium states in thermal bath: \\
$W = \Delta F + W_\text{diss.}$, \qquad $W_\text{diss.} \geq 0$ \\
F: forward $A \to B$ \\
R: reverse $B \to A$  \\
$\ev{- W_R} \leq \Delta F \leq \ev{W_F}$ \medskip \\

\textbf{Tasaki-Crooks}: $\frac{p_F(W)}{p_R(-W)} = e^{\beta (W - \Delta F)}$ \\
\textbf{Jarzynski}: $\ev{e^{- \beta W}} = e^{-\beta \Delta F}$ \qquad $\implies \ev{e^{- \beta 
W_\text{diss.}}} = 1$ \medskip \\

Violations of the 2nd law of TD: $p(W \leq \Delta F - nkT) \leq e^{-n}$ \medskip \\


\vfill\null
\columnbreak
\vfill\null

	
\end{multicols}
\end{document}